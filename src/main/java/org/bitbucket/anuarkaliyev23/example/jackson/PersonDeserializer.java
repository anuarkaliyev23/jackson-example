package org.bitbucket.anuarkaliyev23.example.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDate;

public class PersonDeserializer extends StdDeserializer<Person> {
    public PersonDeserializer() {
        super(Person.class);
    }

    @Override
    public Person deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        TreeNode root = jsonParser.getCodec().readTree(jsonParser);
        String firstName = ((JsonNode) root.get("name")).asText();
        String lastName = ((JsonNode) root.get("surname")).asText();
        String birthday = ((JsonNode) root.get("birthday")).asText();
        return new Person(firstName, lastName, LocalDate.parse(birthday));
    }
}
