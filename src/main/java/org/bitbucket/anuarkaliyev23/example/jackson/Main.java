package org.bitbucket.anuarkaliyev23.example.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(new LocalDateSerializer());
        module.addSerializer(new PersonSerializer());
        module.addDeserializer(Person.class, new PersonDeserializer());
        mapper.registerModule(module);


        Person p = new Person("John", "Doe", LocalDate.of(1990, 1, 1));
        System.out.println(mapper.writeValueAsString(p));

        String json = "{\"name\":\"John\",\"surname\":\"Doe\",\"birthday\":\"1990-01-01\"}";
        System.out.println(mapper.readValue(json, Person.class));
    }
}
