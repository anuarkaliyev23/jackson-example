package org.bitbucket.anuarkaliyev23.example.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class PersonSerializer extends StdSerializer<Person> {
    public PersonSerializer() {
        super(Person.class);
    }

    @Override
    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();

        jsonGenerator.writeStringField("name", person.getFirstName());
        jsonGenerator.writeStringField("surname", person.getLastName());
        jsonGenerator.writeObjectField("birthday", person.getBirthday());
        jsonGenerator.writeEndObject();
    }
}
